//
// Created by Malte on 05.12.2018.
//

#include <stdio.h>
#include <string.h>
#include "list.h"

int main() {
    list *meineliste = NULL;

    printf("-----------------------------------------------\n");
    printf("-Stack                                        -\n");
    printf("-----------------------------------------------\n");

    push(&meineliste, "1-one");
    push(&meineliste, "2-two");
    push(&meineliste, "3-three");

    printf("\nprint_list: \n");
    print_list(&meineliste);

    printf("\n");
    printf("pop:%s \n", pop(&meineliste));
    printf("pop:%s \n", pop(&meineliste));
    printf("pop:%s \n", pop(&meineliste));

    printf("\n\n");
    meineliste = NULL;

    printf("-----------------------------------------------\n");
    printf("-FIFO                                         -\n");
    printf("-----------------------------------------------\n");

    push_fifo(&meineliste, "1-one");
    push_fifo(&meineliste, "2-two");
    push_fifo(&meineliste, "3-three");

    printf("\nprint_list: \n");
    print_list(&meineliste);

    printf("\n");
    printf("pop:%s \n", pop(&meineliste));
    printf("pop:%s \n", pop(&meineliste));
    printf("pop:%s \n", pop(&meineliste));

    printf("\n\n");
    meineliste = NULL;

    printf("-----------------------------------------------\n");
    printf("-INSERT                                       -\n");
    printf("-----------------------------------------------\n");

    insert(&meineliste, "2-two");
    insert(&meineliste, "4-four");
    insert(&meineliste, "1-one");
    insert(&meineliste, "3-three");
    insert(&meineliste, "5-five");

    printf("\nprint_list: \n");
    print_list(&meineliste);

    printf("\n\n");

    printf("-----------------------------------------------\n");
    printf("-SEARCH                                       -\n");
    printf("-----------------------------------------------\n");

    search(&meineliste, "xyz");

    printf("\nprint_list: \n");
    print_list(&meineliste);

    printf("\n");

    search(&meineliste, "2-two");

    printf("\nprint_list: \n");
    print_list(&meineliste);

    return 0;
}