//
// Created by Malte on 05.12.2018.
//

#ifndef PR1_BL5_AG2_LIST_H
#define PR1_BL5_AG2_LIST_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define bool int
#define true 1
#define false 0

typedef struct list {
    char *content;
    struct list *next;
} list;

char *pop(list **);

void *push(list **, char *);

void *insert(list **, char *);

bool search(list **, char *);

void print_list(list **);

void *push_fifo(list **, char *);

#endif //PR1_BL5_AG2_LIST_H