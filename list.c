//
// Created by Malte on 05.12.2018.
//
#include "list.h"

char *pop(list **meineliste) {
    char *string = (*meineliste)->content;
    list *neueliste = NULL;

    neueliste = (*meineliste)->next;
    free(meineliste);
    *meineliste = neueliste;

    return string;
}

void *push(list **meineliste, char *string) {
    list *neueliste;
    neueliste = malloc(sizeof(list));
    neueliste->content = string;
    neueliste->next = *meineliste;
    *meineliste = neueliste;

    printf("push:%s \n", neueliste->content);
}

void *insert(list **meineliste, char *string) {
    list *neueliste;
    neueliste = malloc(sizeof(list));
    neueliste->content = string;

    for (; *meineliste != NULL; meineliste = &(*meineliste)->next) {
        if (strcmp((*meineliste)->content, neueliste->content) > 0) break;
    }

    neueliste->next = *meineliste;
    printf("insert:%s \n", neueliste->content);
    *meineliste = neueliste;
}

bool search(list **meineliste, char *string) {
    list **actual = meineliste;
    list **prev = actual;

    printf("search:%s \n", string);

    if (*actual == NULL) {
        printf("not found \n");
        return false;
    }

    while ((*actual)->next != NULL) {
        if (strcmp((*actual)->content, string) == 0) {
            printf("found: %s \n", string);

            (*prev)->next = (*actual)->next;
            free(actual);
            return true;
        }
        prev = actual;
        actual = &(*actual)->next;
    }
    printf("not found \n");
    return false;
}

void print_list(list **meineliste) {
    list **actual = meineliste;
    int i = 0;

    while (*actual != NULL) {
        printf("%i:%s \n", i++, (*actual)->content);
        actual = &(*actual)->next;
    }
}

void *push_fifo(list **meineliste, char *string) {
    list **aktuelleliste = meineliste;

    if ((*aktuelleliste) == NULL) {
        (*aktuelleliste) = malloc(sizeof(list));
        (*aktuelleliste)->content = string;
        (*aktuelleliste)->next = NULL;
        printf("push_fifo:%s \n", (*aktuelleliste)->content);
    } else {
        while ((*aktuelleliste)->next != NULL) {
            aktuelleliste = &(*aktuelleliste)->next;
        }

        (*aktuelleliste)->next = malloc(sizeof(list));
        (*aktuelleliste)->next->content = string;
        (*aktuelleliste)->next->next = NULL;
        printf("push_fifo:%s \n", (*aktuelleliste)->next->content);
    }

    meineliste = &(*aktuelleliste);
}